import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../models/Post';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;

  constructor() { }

  ngOnInit() {
  }

  getColor() {
    if (this.post.loveIts === 0){
      return '';
    }
    return (this.post.loveIts > 0) ? 'green' : 'red';
  }

  increment() {
    this.post.loveIts++;
  }

  decrement() {
    this.post.loveIts--;
  }

}
