import { Component } from '@angular/core';
import { Post } from './models/Post';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  posts: Post[];

  constructor() {
    this.posts = [
      {
        title: 'Mon premier post',
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         Vestibulum in pulvinar lacus. Quisque vitae dictum massa, vitae ultricies elit. 
         Etiam dapibus bibendum maximus. Maecenas iaculis metus a condimentum maximus. Nunc arcu nulla, 
         sollicitudin sit amet augue sed, aliquam tincidunt nisi,`,
        loveIts: 1,
        createdAt: new Date()
      },
      {
        title: 'Mon deuxième post',
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Vestibulum in pulvinar lacus. Quisque vitae dictum massa, vitae ultricies elit. 
        Etiam dapibus bibendum maximus. Maecenas iaculis metus a condimentum maximus. Nunc arcu nulla, 
        sollicitudin sit amet augue sed, aliquam tincidunt nisi,`,
        loveIts: -1,
        createdAt: new Date()
      },
      {
        title: 'Encore un post',
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Vestibulum in pulvinar lacus. Quisque vitae dictum massa, vitae ultricies elit. 
        Etiam dapibus bibendum maximus. Maecenas iaculis metus a condimentum maximus. Nunc arcu nulla, 
        sollicitudin sit amet augue sed, aliquam tincidunt nisi,`,
        loveIts: 0,
        createdAt: new Date()
      }
    ];
  }

}
